@startuml

title "Documents and operations"


package "Документы" as RawDocuments {

    class "Доход от основной деятельности" as RawIncome {
        restaurant: Restaurant
        cash_flow_article: CashFlowArticle
    }

    RawIncome --> FUNDS_DIVISION
    RawIncome --> CashIncomeOperation
    RawIncome --> NonCashIncomeOperation
    RawIncome --> DiscountIncomeOperation
    RawIncome --> IncomeOperation

    class "Операции с наличными" as RawCash {
        restaurant: Restaurant
        cash_flow_article: CashFlowArticle
        contractor: Contractor
    }

    RawCash --> FUNDS_DIVISION
    RawCash --> CashReceiptsContractorOperation
    RawCash --> CashReceiptsRestaurantOperation
    RawCash --> CashTransferRestaurantOperation
    RawCash --> OtherCashReceiptsOperation
    RawCash --> OtherCashExpenseOperation

    class "Зарплата" as RawSalary {
        restaurant: Restaurant
        cash_flow_article: CashFlowArticle
        employee: Employee
    }

    RawSalary --> SETTLEMENTS_DIVISION
    RawSalary --> PayrollOperation

    class "Списание с расчетного счета" as RawNonCashExpense {
        restaurant: Restaurant
        cash_flow_article: CashFlowArticle
        contractor: Contractor
    }

    RawNonCashExpense --> SETTLEMENTS_DIVISION
    RawNonCashExpense --> NonCashExpenseOperation

    class "Поступление на расчетный счет" as RawNonCashReceipt {
        restaurant: Restaurant
        cash_flow_article: CashFlowArticle
        contractor: Contractor
    }

    RawNonCashReceipt --> SETTLEMENTS_DIVISION
    RawNonCashReceipt --> NonCashReceiptOperation
}

package "Операции" as Operations {

    class "Поступление наличных в кассу от дохода" as CashIncomeOperation {
        debit: 50
        credit: 90
        cash_flow_article: CashFlowArticle
        restaurant: Restaurant
    }

    class "Поступление денег на расчетный счет от дохода" as NonCashIncomeOperation {
        debit: 51
        credit: 90
        cash_flow_article: CashFlowArticle
        restaurant: Restaurant
    }

    class "Неоплата дохода" as DiscountIncomeOperation {
        debit: "??"
        credit: 90
        cash_flow_article: CashFlowArticle
        restaurant: Restaurant
    }

    class "Доход от основной деятельности" as IncomeOperation {
        debit: 90
        credit: "??"
        cash_flow_article: CashFlowArticle
        restaurant: Restaurant
        currency: Currency
    }

    class "Приход денег от контрагента" as CashReceiptsContractorOperation {
        debit: 50
        credit: "??"
        cash_flow_article: CashFlowArticle
        restaurant: Restaurant
        contractor: Contractor
    }

    class "Приход денег от другого ресторана" as CashReceiptsRestaurantOperation {
        debit: 50
        credit: "??"
        restaurant: Restaurant
    }

    class "Перевод денег в другой ресторан" as CashTransferRestaurantOperation {
        debit: "??"
        credit: 50
        restaurant: Restaurant
    }

    class "Прочие поступления наличных" as OtherCashReceiptsOperation {
        debit: 50
        credit: "??"
        restaurant: Restaurant
        cash_flow_article: CashFlowArticle
    }

    class "Прочие расходы наличных" as OtherCashExpenseOperation {
        debit: "20 ??"
        credit: 50
        restaurant: Restaurant
        cash_flow_article: CashFlowArticle
    }

    class "Начисление зарплаты" as PayrollOperation {
        debit: 70
        credit: 50
        restaurant: Restaurant
        cash_flow_article: CashFlowArticle
        employee: Employee
        position: Position
    }

    class "Списание с расчетного счета" as NonCashExpenseOperation {
        debit: "??"
        credit: 51
        restaurant: Restaurant
        cash_flow_article: CashFlowArticle
        contractor: Contractor
    }

    class "Поступление на расчетный счет" as NonCashReceiptOperation {
        debit: 51
        credit: "??"
        restaurant: Restaurant
        cash_flow_article: CashFlowArticle
        contractor: Contractor
    }
}

package "Разделы учета" as ACCOUNT_DIVISIONS {

    package "1 Внеоборотные активы" as NON_CURRENT_ASSETS_DIVISION {
        class "1 Основные средства" as FIXED_ASSETS_ACCOUNT {
        }
    }

    package "2 Производственные запасы" as  PRODUCTIVE_RESERVES_DIVISION{
        class "10 Материалы" as  MATERIALS_ACCOUNT{
        }
    }

    package "3 Затраты на производство" as  PRODUCTION_COSTS_DIVISION{
        class "20 Основное производство" as  PRIMARY_PRODUCTION_ACCOUNT{
        }
        class "25 Общепроизводственные расходы" as  GENERAL_PRODUCTION_COSTS_ACCOUNT{
        }
        class "26 Общехозяйственные расходы" as  GENERAL_RUNNING_COSTS_ACCOUNT{
        }
        class "28 Брак в производстве" as  DEFECT_IN_PRODUCTION_ACCOUNT{
        }
    }

    package "4 Готовая продукция" as  FINISHED_PRODUCTS_AND_GOODS_DIVISION{
        class "40 Выпуск продукции работ, услуг" as  RELEASE_OF_PRODUCTS_ACCOUNT{
        }
        class "41 Товары" as  PRODUCTS_ACCOUNT{
        }
        class "43 Готовая продукция" as  FINISHED_PRODUCTS_ACCOUNT{
        }
        class "45 Товары отгруженные" as  GOODS_SHIPPED_ACCOUNT{
        }
    }

    package "5 Денежные средства" as  FUNDS_DIVISION{
        class "50 Касса" as  CASHBOX_ACCOUNT{
        }
        class "51 Расчетные счета" as  SETTLEMENT_ACCOUNTS_ACCOUNT{
        }
        class "57 Переводы в пути" as  TRANSFERS_ON_THE_WAY_ACCOUNT{
        }
    }

    package "6 Расчеты" as  SETTLEMENTS_DIVISION{
        class "60 Расчеты с поставщиками и подрядчиками" as  SETTLEMENTS_WITH_SUPPLIERS_ACCOUNT{
        }
        class "62 асчеты с покупателями и заказчиками" as  SETTLEMENTS_WITH_CUSTOMERS_ACCOUNT{
        }
        class "70 Расчеты с персоналом по оплате труда" as  PAYMENTS_TO_PERSONNEL_FOR_WAGES_ACCOUNT{
        }
        class "71 Расчеты с подотчетными лицами" as  CALCULATIONS_WITH_ACCOUNTABLE_PERSONS_ACCOUNT{
        }
        class "75 Расчеты с учредителями" as  SETTLEMENTS_WITH_FOUNDERS_ACCOUNT{
        }
    }

    package "7 Капитал" as  COMPANY_CAPITAL_DIVISION{
        class "80 Уставный капитал" as  AUTHORIZED_CAPITAL_ACCOUNT{
        }
        class "84 Нераспределенная прибыль (убыток)" as  UNDESTRIBUTED_PROFITS_ACCOUNT{
        }
    }

    package "8 Финансовый результат" as  FINANCIAL_RESULTS_DIVISION{
        class "90 Продажи" as  SALES_ACCOUNT{
        }
        class "91 Прочие доходы и расходы" as  OTHER_INCOME_AND_EXPENSES_ACCOUNT{
        }
        class "99 Прибыли и убытки" as  PROFIT_AND_LOSS_ACCOUNT{
        }
    }
}

@enduml
